import Vue from "vue";
import HeaderComponent from "@/components/header/header.component.vue";
import TodoListComponent from "@/components/todo-list/todo-list.component.vue";
import AddTaskComponent from "@/components/todo-list/add-task/add-task.component.vue";
import TasksListComponent from "@/components/todo-list/tasks-list/tasks-list.component.vue";

Vue.component("kta-header", HeaderComponent);
Vue.component("kta-todo-list", TodoListComponent);
Vue.component("kta-add-task", AddTaskComponent);
Vue.component("kta-tasks-list", TasksListComponent);
